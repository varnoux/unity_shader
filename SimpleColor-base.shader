﻿// UNITY_SHADER_NO_UPGRADE
Shader "Custom/SimpleColor" 
{
    Properties
    {
		_Color("Couleur",Color) = (1,1,1,1)
		_MyTexture("Texture", 2D) = "white" {}
    }



    SubShader
    {        
		Pass
		{

			Tags
			{
				"LightMode" = "ShadowCaster"
			}

			CGPROGRAM

			#pragma target 3.0
			#pragma vertex   main_vert
			#pragma fragment main_frag
			#include "UnityCG.cginc"

			struct a2v
			{
				float4  vertex : POSITION;
			};

			struct v2f
			{
				float4  pos : SV_POSITION;
			};

			sampler2D _MyTexture;
			float4 _MyTexture_ST;

			v2f main_vert(a2v v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			fixed4 _Color;
			uniform sampler2D _ShadowMapTexture;

			half4 main_frag(v2f i) : COLOR
			{
				return float4(1,1,1,1);
			}

			ENDCG
		}

		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
				LOD 100

				ZWrite Off
				Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
				

			Tags
			{
				"LightMode" = "ForwardBase"
			}

            CGPROGRAM

			#pragma target 3.0
            #pragma vertex   main_vert
            #pragma fragment main_frag
			#include "UnityCG.cginc"

		

				struct material
			{
				float3  ka; // Coefficient de réflexion ambiente.
				float3  kd; // Coefficient de réflexion diffuse.
				float3  ks; // Coefficient de réflexion spéculaire.
				float re; // Réflectivité.
			};

			// Données stockées au point d'intersection rayon / objet. 
			// Cf. fonction cast_ray() ci-dessous.
			struct intersection
			{
				float3     p;            // Position de l'intersection (espace de la scène).
				float3     n;            // Normale de l'objet au point d'intersection.
				material mat;          // Couleur de l'objet intersecté.
				bool     intersection; // Vrai si une intersection a eu lieu.
				int      object;       // Index de l'objet intersecté.
				float pas;
			};

            struct a2v
            {
                float4  vertex : POSITION;
				float4 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
				float3 N : NORMAL;
				float4 pos_n : TEXCOORD2;
				float4 shadowCoord : TEXCOORD1;
            };

			sampler2D _MyTexture;
			float4 _MyTexture_ST;
			uniform float4x4 _Projector;

            v2f main_vert( a2v v)
            {
				v2f o;
				o.shadowCoord = mul(UNITY_MATRIX_MVP, v.vertex);
				o.pos_n = v.vertex;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = float4((v.texcoord.xy + _MyTexture_ST.zw) * _MyTexture_ST.xy,0,0);
				o.N = v.normal;
				return o;
            }

			fixed4 _Color;
			uniform sampler2D _ShadowMapTexture;
			uniform float camProf;
			uniform float3 lumPos;
			uniform float Seuil;
			uniform float time;
			uniform bool animate;
			uniform bool shadows;
			uniform float scaleDensity;
			uniform int nbPas;

			// Les vecteurs suivants sont fournis par Render Monkey.
			// Ils sont définis dans l'espace de la scène (espace monde).
			uniform float4  O; // Position de l'observateur.
			uniform float4  D; // Direction d'observation.
			uniform float4  i; // Vecteur droit (x) de l'écran.
			uniform float4  j; // Vecteur haut  (y) de l'écran.


			// Par le demomaker Las/Mercury.
			// Retourne une densité en fonction d’un point 3D.
			// La densité peut être négative.
			float noise(float3 p)
			{
				float3 new_p;

				new_p = p * 6.0;	

				float3 i = floor(new_p);
				float4 a = dot(i, float3(1., 57., 21.)) + float4(0., 57., 21., 78.);
				float3 f = (cos((new_p - i)*acos(-1.))*(-.5) + .5);
				a = lerp(sin(cos(a)*a), sin(cos(1. + a)*(1. + a)), f.x);
				a.xy = lerp(a.xz, a.yw, f.y);
				return lerp(a.x, a.y, f.z);
			}

			// Calcule le point d'intersection entre une sphère (sph) et un rayon (ro, rd).
			// ro  : Origine du rayon (point 3d dans l'espace de la scène).
			// rd  : Direction du rayon (vecteur dans l'espace de la scène).
			// sph : Paramètres de la sphère.
			// Retourne la coordonnée paramétrique du point d'intersection sur le rayon.
			float2 iSphere(in float3 ro, in float3 rd)
			{
				float4 objectOrigin = mul(unity_ObjectToWorld, float4(0.0, 0.0, 0.0, 1.0));

				float3 oc = ro - objectOrigin.xyz;

				// a = dot(rd,rd) = 1
				float b = 2.0 * dot(oc, rd);
				float c = dot(oc, oc) - 1;

				// Déterminant : b2 - 4ac
				float d = b * b - 4.0 * c;

				if (d <= 0.0)
					return -1.0;

				// Autre racine t2 > t => point sortant de la sphère => actuellement ignoré.
				float t1 = (-b - sqrt(d)) / 2.0;
				float t2 = (-b + sqrt(d)) / 2.0;

				if (t2 <= 0)
					return -1.0;

				if (t1 < 0)
					t1 = 0.;

				float t = -1.;
				float pas = (t2 - t1) / 400;

				for (float i = t1; i <= t2; i += pas) {
					float3 p = (ro + rd * i);
					float density = noise(p);
					if (density > 0.5) {
						t = i;
						break;
					}
				}

				return float2(t, pas);
			}

			// Retourne la normale en un point pos de la surface de la sphère sph.
			float3 nSphere(in float3 pos, float pas)
			{
				float4 objectOrigin = mul(unity_ObjectToWorld, float4(0.0, 0.0, 0.0, 1.0));

				float test = 2;
				if (length(pos - objectOrigin.xyz) >= 1. - pas)
					return (pos - objectOrigin.xyz) / 1.;
				else
					return normalize(
						float3(
							noise(pos - float3(test, 0, 0)) - noise(pos + float3(test, 0, 0)),
							noise(pos - float3(0, test, 0)) - noise(pos + float3(0, test, 0)),
							noise(pos - float3(0, 0, test)) - noise(pos + float3(0, 0, test))
							)
					);
			}
			
			// Lancé de rayon.
			// Cette fonction retourne le point d'intersection le plus proche de l'observateur 
			// entre le rayon (ro, rd) et les objets de la scène.
			intersection cast_ray(in float3 ro, in float3 rd)
			{
				// Intialisation de l'intersection.
				intersection i;
				i.p = float3(0, 0, 0);
				i.n = float3(0, 0, 0);

				material m;
				m.ka = float3(0, 0, 0);
				m.kd = float3(0, 0, 0);
				m.ks = float3(0, 0, 0);
				m.re = 0.f;

				i.mat = m;
				i.intersection = false;
				i.object = -1;
				i.pas = 0.f;

				float t = 10000.0;

				float t0;

				// Calcul de l'intersection rayon / sphère.
				float2 infos = iSphere(ro, rd);

				t0 = infos.x;

				// S'il y a eu intersection plus proche que la précédente.
				if (t0 > 0.0 && t0 < t)
				{
					// Stocke les données de l'intersection.
					i.p = ro + t0 * rd;
					i.n = nSphere(i.p, infos.y);
					i.intersection = true;
					i.pas = infos.y;
					t = t0;
				}
				return i;
			}


			float3 trace(in float3 ro, in float3 rd)
			{
				float3 col = float3(0,0,0);

				// Lancé de rayon.
				intersection i = cast_ray(ro, rd);

				// S'il y a eu intersection, la couleur du rayon = couleur de l'objet intersecté,
				// sinon, la couleur = noir.
				if (i.intersection) {

					float3 lumDir = normalize(_WorldSpaceLightPos0.xyz - i.p);

					//intersection shadow = cast_ray(i.p + i.n * i.pas, lumDir);

					float3 ka = float3(0.01,0.01,0.01);
					col += ka;

					//if (!shadow.intersection) {

						float3 kd = float3(0.9, 0.0, 0.0);
						//float3 ks = float3(0.5,0.5,0.5);

						//float3 reflectLum = normalize(reflect(-lumDir, i.n));


						col += kd * max(dot(i.n, lumDir), 0.);
						//col += ks * pow(max(dot(reflectLum, -rd), 0.), 10);
					//}
						col = i.n;
				}

				return col;
			}

			half4 main_frag(v2f k) : COLOR
			{
				O = float4(_WorldSpaceCameraPos, 1); // Position de l'observateur.
				D = normalize(-UNITY_MATRIX_V[2]); // Direction d'observation.
				i = normalize(UNITY_MATRIX_V[0]); // Vecteur droit (x) de l'écran.
				j = normalize(UNITY_MATRIX_V[1]); // Vecteur haut  (y) de l'écran.

				// Convertit la coordonnée (x,y) du fragment en espace écran, afin de calculer 
				//  la direction du rayon en espace scène.
				float2 q = k.pos.xy / _ScreenParams.xy; // Coordonnée du fragment entre 0 et 1
				float2 p = -1.0 + 2.0 * q;                   // => -1..1
				p.x *= _ScreenParams.x / _ScreenParams.y;        // Prise en compte du ratio de l'écran.

				// Direction du rayon en espace scène.
				float3 rd = normalize(p.x * i.xyz + p.y * j.xyz + 1.5 * D.xyz);

				//float3 rd = normalize(k.pos_n.xyz - O.xyz);
				
				//return float4(rd,1.0);

				// Lance un rayon d'origine O, dans la direction rd, dans la scène.
				float3 col = trace(O.xyz, rd);
				
				if(col.x == 0 && col.y == 0 && col.z == 0)
					return float4(col, 0.0);

				return float4(col, 1.0);
            }
            
            ENDCG
        }
    }
}